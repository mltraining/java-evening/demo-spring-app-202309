insert into quote (id, category, text) values (nextval('quote_seq'), 'LIFE', 'We must all suffer from one of two pains: the pain of discipline or the pain of regret.');
insert into quote (id, category, text) values (nextval('quote_seq'), 'MOTIVATION', 'May the force be with you.');

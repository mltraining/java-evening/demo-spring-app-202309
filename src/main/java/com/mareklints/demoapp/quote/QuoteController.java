package com.mareklints.demoapp.quote;

import com.mareklints.demoapp.quote.dto.QuoteChangeRequest;
import com.mareklints.demoapp.quote.model.Quote;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/quotes")
@RequiredArgsConstructor
@CrossOrigin("*")
public class QuoteController {

    // Dependency injection (inversion of control)
    private final QuoteService quoteService; // final - kohustuslik

    // See on samaväärne @RequiredArgsConstrucor annotatsiooniga.
//    public QuoteController(QuoteService quoteService) {
//        this.quoteService = quoteService;
//    }

    @PostMapping("/quote")
    public void addQuote(@RequestBody QuoteChangeRequest request) {
        quoteService.addNewQuote(request);

//        quoteService.saveNewQuoteToMemory(quote);
    }

    @PutMapping("/quote/{id}")
    public void updateQuote(
            @PathVariable("id") Integer id,
            @RequestBody QuoteChangeRequest request) {
        quoteService.updateExistingQuote(id, request);
    }

    @GetMapping
    public List<Quote> getAllQuotes() {
        return quoteService.getAllQuotesFromMemory();
    }

    @GetMapping("/quote/{id}")
    public Quote getQuote(@PathVariable("id") Integer id) {
        return quoteService.getOneQuoteById(id);
    }

    @DeleteMapping("/quote/{id}")
    public void deleteQuote(@PathVariable("id") Integer id) {
        quoteService.deleteOneQuoteById(id);
    }
}

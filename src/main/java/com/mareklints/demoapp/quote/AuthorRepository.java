package com.mareklints.demoapp.quote;

import com.mareklints.demoapp.quote.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepository extends JpaRepository<Author, Integer> {

    Author findByName(String authorName);
}

package com.mareklints.demoapp.quote.dto;

import lombok.Data;

@Data
public class QuoteChangeRequest {
    private String category;
    private String text;
    private String authorName;
}

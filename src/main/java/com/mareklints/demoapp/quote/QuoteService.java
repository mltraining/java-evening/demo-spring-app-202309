package com.mareklints.demoapp.quote;

import com.mareklints.demoapp.quote.dto.QuoteChangeRequest;
import com.mareklints.demoapp.quote.model.Author;
import com.mareklints.demoapp.quote.model.Quote;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class QuoteService {

    private final QuoteRepository quoteRepository;
    private final AuthorRepository authorRepository;

//    private static List<Quote> quotes = new ArrayList<>();

    @Transactional
    public void saveNewQuoteToMemory(Quote quote) {
//        quotes.add(quote);
        quoteRepository.saveAndFlush(quote);
    }

    @Transactional
    public void addNewQuote(QuoteChangeRequest request) {
        // Kas autor tuleb ka salvestada?
        Author author = getOrCreateAuthor(request);

        Quote quote = new Quote();
        quote.setCategory(request.getCategory());
        quote.setText(request.getText());
        quote.setAuthor(author);
        quoteRepository.saveAndFlush(quote);
    }

    private Author getOrCreateAuthor(QuoteChangeRequest request) {
        Author author = null;
        if (request.getAuthorName() != null) {
            // Tahame autorit ka salvestada.
            // Salvestame autori.
            author = authorRepository.findByName(request.getAuthorName());
            if (author == null) { // Autorit baasist ei leitud, lisame selle
                author = new Author();
                author.setName(request.getAuthorName());
                author = authorRepository.saveAndFlush(author);
            }
        }
        return author;
    }

    @Transactional
    public void updateExistingQuote(Integer id, QuoteChangeRequest request) {
        // Tarkus uuendamisel on siin...
        if (quoteRepository.existsById(id)) {
            Quote existingQuote = quoteRepository.findById(id).get();
            if (request.getCategory() != null) {
                existingQuote.setCategory(request.getCategory());
            }
            if (request.getText() != null) {
                existingQuote.setText(request.getText());
            }
            Author author = getOrCreateAuthor(request);
            existingQuote.setAuthor(author);

            quoteRepository.saveAndFlush(existingQuote);
        }
    }

    @Transactional(readOnly = true)
    public List<Quote> getAllQuotesFromMemory() {
//        return quotes;
        return quoteRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Quote getOneQuoteById(Integer id) {
        return quoteRepository.findById(id).get(); // get - võtame kommi paberist välja
    }

    @Transactional
    public void deleteOneQuoteById(Integer id) {
        quoteRepository.deleteById(id);
    }
}

package com.mareklints.demoapp.quote.model;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "quote")
public class Quote {
//    {
//        "category": "LIFE",
//        "text": "In order to write about life first you must live it."
//    }

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "category")
    private String category;

    @Column(name = "text")
    private String text;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private Author author;
}

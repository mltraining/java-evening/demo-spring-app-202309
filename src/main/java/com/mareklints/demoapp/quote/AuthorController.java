package com.mareklints.demoapp.quote;

import com.mareklints.demoapp.quote.model.Author;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/authors")
@RequiredArgsConstructor
@CrossOrigin("*")
public class AuthorController {

    private final AuthorService authorService;

    // Semantiliselt sama, mis @RequiredArgsConstructor
//    public AuthorController(AuthorService authorService) {
//        this.authorService = authorService;
//    }

    @GetMapping
    public List<Author> getAuthors() {
        return authorService.getAuthors();
    }


}

package com.mareklints.demoapp.quote;

import com.mareklints.demoapp.quote.model.Author;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AuthorService {

    private final AuthorRepository authorRepository;

//    public AuthorService(AuthorRepository authorRepository) {
//        this.authorRepository = authorRepository;
//    }

    @Transactional(readOnly = true)
    public List<Author> getAuthors() {
        return authorRepository.findAll();
    }


}

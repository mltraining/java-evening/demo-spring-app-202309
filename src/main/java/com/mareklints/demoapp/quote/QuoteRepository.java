package com.mareklints.demoapp.quote;


import com.mareklints.demoapp.quote.model.Quote;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuoteRepository extends JpaRepository<Quote, Integer> {
}

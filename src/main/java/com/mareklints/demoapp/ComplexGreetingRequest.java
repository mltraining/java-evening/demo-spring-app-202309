package com.mareklints.demoapp;

import lombok.Data;

@Data
//@AllArgsConstructor
//@NoArgsConstructor
public class ComplexGreetingRequest {
//    {
//        "greetingText": "See siin on üks pikemat sorti tervitus!",
//        "greetingFlavour": "NEUTRAL",
//        "value": 10
//    }

    private String greetingText;
    private String greetingFlavour;
    private Integer value;

//    public String getGreetingText() {
//        return greetingText;
//    }
//
//    public void setGreetingText(String greetingText) {
//        this.greetingText = greetingText;
//    }
//
//    public String getGreetingFlavour() {
//        return greetingFlavour;
//    }
//
//    public void setGreetingFlavour(String greetingFlavour) {
//        this.greetingFlavour = greetingFlavour;
//    }
//
//    public Integer getValue() {
//        return value;
//    }
//
//    public void setValue(Integer value) {
//        this.value = value;
//    }
}

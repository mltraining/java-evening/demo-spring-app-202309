package com.mareklints.demoapp;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/demo")
public class DemoController {

    //    http://localhost:8080/demo?greeting=tere
    @GetMapping
    public String sayHello(@RequestParam("greeting") String greeting) {
        return "Brauser ütles: " + greeting;
    }


    //    http://localhost:8080/demo/greeting2/teretere?mode=11
    @GetMapping("/greeting2/{greeting}")
    public String sayHello2(@PathVariable("greeting") String greeting, @RequestParam("mode") Integer mode) {
        return "Brauser ütles: " + greeting + ", mode=" + mode;
    }

    @PostMapping("/complexgreeting")
    public ComplexGreetingRequest sayComplexGreeting(@RequestBody ComplexGreetingRequest complexGreetingRequest) {
        complexGreetingRequest.setGreetingText("See on Java rakenduse sees muudetud tekst!");
        return complexGreetingRequest;
    }
}
